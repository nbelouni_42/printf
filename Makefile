# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/05 17:19:47 by nbelouni          #+#    #+#              #
#    Updated: 2015/04/29 22:33:04 by nbelouni         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME= libftprintf.a
CC= gcc
CFLAGS= -Wall -Werror -Wextra
HFLAGS= 
HDS= 
OBJ=ft_bzero.o ft_isalnum.o ft_isalpha.o ft_isascii.o ft_isdigit.o\
	ft_isprint.o ft_memccpy.o ft_memchr.o ft_memcmp.o ft_memcpy.o\
	ft_memmove.o ft_memset.o ft_putstr.o ft_strcat.o ft_strchr.o ft_strcmp.o\
	ft_strcpy.o ft_strdup.o ft_strlcat.o ft_strlen.o ft_strncat.o\
	ft_strncmp.o ft_strncpy.o ft_strnstr.o ft_strrchr.o ft_strstr.o\
	ft_tolower.o ft_toupper.o ft_atoi.o ft_memalloc.o ft_memdel.o\
	ft_strnew.o ft_strdel.o ft_strclr.o	ft_striter.o ft_striteri.o\
	ft_strmap.o ft_strmapi.o ft_strequ.o ft_strnequ.o ft_strsub.o\
	ft_strjoin.o ft_strtrim.o ft_strsplit.o ft_itoa.o ft_putchar.o\
	ft_putendl.o ft_putnbr.o ft_putchar_fd.o ft_putstr_fd.o ft_putnbr_fd.o\
	ft_putendl_fd.o ft_lstnew.o ft_lstdelone.o ft_lstdel.o ft_lstadd.o\
	ft_lstiter.o ft_lstmap.o ft_strfjoin.o ft_strbchr.o ft_blanksplit.o\
	ft_free_tab.o ft_isblank.o ft_strchr_blank.o ft_lstadd_end.o ft_ltoa.o\
	ft_lltoa.o ft_stoa.o ft_uitoa.o ft_ustoa.o ft_ultoa.o ft_putnstr.o\
	ft_xtoa.o ft_xltoa.o ft_xlltoa.o ft_xstoa.o ft_ctoa.o ft_xctoa.o ft_octoa.o\
	ft_otoa.o ft_oltoa.o ft_olltoa.o ft_ostoa.o ft_uctoa.o ft_ulltoa.o\
	ft_strndup.o ft_putstr_col.o ft_tabdup.o ft_newtab.o aff_tab.o\
	arg_check.o convert_c.o convert_d.o convert_o.o ft_printf.o\
	convert_s.o convert_u.o convert_x.o format_convert.o prec_signed_nbr.o\
	prec_unsigned_nbr.o prec_other.o prec_string.o realloc_tab.o\
	realloc_unsigned_tab.o prec_sort.o
SRCS=src/ft_bzero.c src/ft_isalnum.c src/ft_isalpha.c src/ft_isascii.c src/ft_isdigit.c\
	 src/ft_isprint.c src/ft_memccpy.c src/ft_memchr.c src/ft_memcmp.c src/ft_memcpy.c\
	 src/ft_memmove.c src/ft_memset.c src/ft_putstr.c src/ft_strcat.c src/ft_strchr.c\
	 src/ft_strcmp.c src/ft_strcpy.c src/ft_strdup.c src/ft_strlcat.c src/ft_strlen.c\
	 src/ft_strncat.c src/ft_strncmp.c src/ft_strncpy.c src/ft_strnstr.c src/ft_strrchr.c\
	 src/ft_strstr.c src/ft_tolower.c src/ft_toupper.c src/ft_atoi.c src/ft_memalloc.c\
	 src/ft_memdel.c src/ft_strnew.c src/ft_strdel.c src/ft_strclr.c\
	 src/ft_striter.c src/ft_striteri.c src/ft_strmap.c src/ft_strmapi.c src/ft_strequ.c\
	 src/ft_strnequ.c src/ft_strsub.c src/ft_strjoin.c src/ft_strtrim.c src/ft_strsplit.c\
	 src/ft_itoa.c src/ft_putchar.c src/ft_putendl.c src/ft_putnbr.c src/ft_putchar_fd.c\
	 src/ft_putstr_fd.c src/ft_putnbr_fd.c src/ft_putendl_fd.c src/ft_lstnew.c\
	 src/ft_lstdelone.c src/ft_lstdel.c src/ft_lstadd.c src/ft_lstiter.c src/ft_lstmap.c\
	 src/ft_strfjoin.c src/ft_strbchr.c src/ft_blanksplit.c src/ft_free_tab.c src/ft_isblank.c\
	 src/ft_strchr_blank.c src/ft_lstadd_end.c src/ft_ltoa.c src/ft_lltoa.c src/ft_stoa.c\
	 src/ft_uitoa.c src/ft_ultoa.c src/ft_putnstr.c src/ft_ctoa.c src/ft_octoa.c src/ft_xctoa.c\
	 src/ft_xtoa.c src/ft_xltoa.c src/ft_xlltoa.c src/ft_xstoa.c src/ft_ustoa.c src/ft_uctoa.c\
	 src/ft_otoa.c src/ft_oltoa.c src/ft_olltoa.c src/ft_ostoa.c src/ft_ulltoa.c src/ft_strndup.c\
	 src/ft_putstr_col.c src/ft_tabdup.c src/ft_newtab.c src/aff_tab.c\
	 src/arg_check.c src/convert_c.c src/convert_d.c src/convert_o.c src/ft_printf.c\
	 src/convert_s.c src/convert_u.c src/convert_x.c src/format_convert.c src/prec_signed_nbr.c\
	 src/prec_unsigned_nbr.c src/prec_other.c src/prec_string.c src/realloc_tab.c\
	 src/realloc_unsigned_tab.c src/prec_sort.c

all: $(NAME)
$(NAME): $(OBJ) 
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

$(OBJ): $(SRCS)
	$(CC) $(CFLAGS) -c $(SRCS) -I includes/

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: clean all

test:
	gcc $(CFLAGS) testft.c -L includes/ -lftprintf -I includes -I includes/
