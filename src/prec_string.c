/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prec_string.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 19:02:23 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 19:59:42 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	**init_tab(t_fmt *fmt, int len)
{
	int		tab_size;
	int		i;
	char	**new;

	i = 0;
	tab_size = (len > 0) ? 1 : 0;
	if (fmt->prec_p > len)
		tab_size++;
	if (fmt->prec_esp > ((fmt->prec_p > len) ? fmt->prec_p : len))
		tab_size++;
	if (!(new = (char **)malloc(sizeof(char *) * (tab_size + 1))))
		return (NULL);
	while (i <= tab_size)
	{
		new[i] = NULL;
		i++;
	}
	return (new);
}

static int	total_len(t_fmt *fmt, char **s)
{
	int		i;
	int		len;

	len = 0;
	i = 0;
	if (s[0][0] == 0)
		len = 1;
	else if (fmt->type == 'S' || (fmt->type == 's' && fmt->l))
	{
		while (s[i])
		{
			len += ft_strlen(s[i]);
			if ((fmt->prec_p > 0 || fmt->is_point) && len > fmt->prec_p)
				ft_memset(s[i], 0, 4);
			i++;
		}
	}
	else
		len = ft_strlen(s[0]);
	return (len);
}

static char	*w_to_s(t_fmt *fmt, char **s)
{
	char	*tmp[0];
	int		i;

	i = 1;
	tmp[0] = NULL;
	if (fmt->type == 'S' || (fmt->type == 's' && fmt->l))
	{
		tmp[0] = ft_strdup(s[0]);
		while (s[i])
		{
			tmp[0] = ft_strjoin(tmp[0], s[i]);
			i++;
		}
	}
	return (tmp[0]);
}

char		*add_c(t_fmt *fmt, char c, int len)
{
	char	*esp;
	int		esp_len;

	esp = NULL;
	if (esp_len = 0, len == 0 && (!fmt->is_point || fmt->type == 'c'))
		++len;
	if (c == '0')
	{
		if (fmt->zero && fmt->is_point)
			esp_len = fmt->prec_esp - len;
		else
			esp_len = fmt->prec_p - len;
		if (!(esp = (char *)malloc(sizeof(char) * esp_len + 1)))
			return (NULL);
		ft_memset(esp, c, esp_len);
	}
	else
	{
		esp_len = fmt->prec_esp - len;
		if (!(esp = (char *)malloc(sizeof(char) * esp_len + 1)))
			return (NULL);
		ft_memset(esp, c, esp_len);
	}
	esp[esp_len] = 0;
	return (esp);
}

char		**add_prec_str(t_fmt *fmt, char **s)
{
	char	**new;
	char	**tmp;
	int		len;

	tmp = ft_newtab(3);
	len = total_len(fmt, s);
	if (!(tmp[0] = w_to_s(fmt, s)))
		tmp[0] = ft_strdup(s[0]);
	len = ft_strlen(ft_strcmp("(null)", s[0]) ? tmp[0] : 0);
	if (fmt->is_point && !fmt->prec_p && len != 1)
		tmp[0] = ft_strdup("");
	else if (fmt->type != 'S' && fmt->prec_p > 0 && fmt->prec_p < len)
		tmp[0] = ft_strndup(s[0], fmt->prec_p - 1);
	if (fmt->zero && (fmt->prec_p > len ||
	(fmt->is_point && fmt->prec_esp > len)))
		tmp[1] = add_c(fmt, '0', len);
	len = ft_strlen(tmp[0]);
	if (!fmt->zero && len < fmt->prec_esp)
		tmp[2] = add_c(fmt, ' ', len + ft_strlen(tmp[1]));
	new = init_tab(fmt, len);
	new = prec_sort(fmt, tmp, new);
	return (new);
}
