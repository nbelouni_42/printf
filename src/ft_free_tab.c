/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/03 20:38:53 by nbelouni          #+#    #+#             */
/*   Updated: 2015/03/20 14:30:52 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			**ft_free_ctab(char **tab)
{
	size_t	i;

	i = 0;
	if (!tab || !*tab)
		return (NULL);
	while (tab[i])
	{
		free(tab[i]);
		tab[i] = NULL;
		i++;
	}
	free(tab);
	tab = NULL;
	return (NULL);
}

unsigned char	**ft_free_uctab(unsigned char **tab)
{
	size_t	i;

	i = 0;
	if (!tab || !*tab)
		return (NULL);
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
	tab = NULL;
	return (NULL);
}

int				**ft_free_itab(int **tab, size_t size)
{
	size_t	i;

	i = 0;
	if (!tab || !*tab)
		return (NULL);
	while (i < size)
	{
		free(tab[i]);
		i++;
	}
	free(tab);
	tab = NULL;
	return (NULL);
}
