/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/08 16:27:40 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 17:50:59 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			wchar_to_string(unsigned char **s, wchar_t c)
{
	int	i;

	if (i = 0, c < (1 << 7))
		s[0][i++] = (unsigned char)(c);
	else if (c < (1 << 11))
	{
		s[0][i++] = (unsigned char)((c >> 6) | 0xC0);
		s[0][i++] = (unsigned char)((c & 0x3F) | 0x80);
	}
	else if (c < (1 << 16))
	{
		s[0][i++] = (unsigned char)(((c >> 12)) | 0xE0);
		s[0][i++] = (unsigned char)(((c >> 6) & 0x3F) | 0x80);
		s[0][i++] = (unsigned char)((c & 0x3F) | 0x80);
	}
	else if (c < (1 << 21))
	{
		s[0][i++] = (unsigned char)(((c >> 18)) | 0xF0);
		s[0][i++] = (unsigned char)(((c >> 12) & 0x3F) | 0x80);
		s[0][i++] = (unsigned char)(((c >> 6) & 0x3F) | 0x80);
		s[0][i++] = (unsigned char)((c & 0x3F) | 0x80);
	}
	else
		return (-1);
	return (1);
}

char		**cast_to_ws(wchar_t *s, int *len)
{
	unsigned char	**wchar;
	unsigned char	**b;
	char			**str;
	wchar_t			c;
	int				i;

	if ((i = -1) && !s)
		return (NULL);
	if (!(b = (unsigned char **)malloc(sizeof(unsigned char *) * 2)))
		return (NULL);
	b[0] = (unsigned char *)malloc(sizeof(unsigned char) * 1024);
	b[1] = 0;
	if (!(wchar = (unsigned char **)malloc(sizeof(unsigned char *))))
		return (NULL);
	wchar[0] = NULL;
	while (s[++i])
	{
		ft_memset(b[0], 0, 1024);
		c = s[i];
		if (wchar_to_string(b, c) == -1)
			return (NULL);
		wchar = realloc_utab(wchar, b);
		len++;
	}
	return ((str = (char **)wchar));
}

char		**init_tab(char *line, int size)
{
	char	**tab;

	if (!(tab = (char **)malloc(sizeof(char *) * size)))
		return (NULL);
	if (line)
		tab[0] = ft_strdup(line);
	else
		tab[0] = 0;
	tab[1] = 0;
	return (tab);
}

char		**type_s(va_list pa, t_fmt *fmt, int *len)
{
	char	**s;
	wchar_t	*w;
	char	**s_c;

	s_c = init_tab(NULL, 2);
	if (s = 0, (fmt->type == 's' && fmt->l == 1) || fmt->type == 'S')
	{
		w = va_arg(pa, wchar_t *);
		if (w && *w)
		{
			s_c = ft_free_ctab(s_c);
			s_c = (cast_to_ws(w, len));
		}
		else
			s_c[0] = (char *)w;
	}
	else
		s_c[0] = va_arg(pa, char *);
	if (s_c && *s_c && !**s_c)
		s = ft_tabdup(NULL);
	else if (s_c && *s_c && **s_c)
		s = ft_tabdup(s_c);
	else
		s = init_tab("(null)", 2);
	return (s);
}

char		**convert_s(va_list pa, t_fmt *fmt)
{
	char	**s;
	int		len;

	len = 0;
	s = type_s(pa, fmt, &len);
	if (s)
		return (s);
	return (NULL);
}
