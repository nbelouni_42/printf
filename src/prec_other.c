/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prec_other.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/04 17:08:46 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 18:39:30 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	len_zero(t_fmt *fmt, char *s)
{
	int		s_len;

	if (s_len = 0, fmt->is_point)
	{
		if (!ft_strcmp(s, "0") && !fmt->prec_p)
			s_len = 1;
		else
			s_len = ft_strlen(s);
		if (fmt->sharp)
			s_len = s_len + ((fmt->type == 'o' || fmt->type == 'O') ? 1 : 0);
	}
	else if (fmt->zero)
	{
		if (!fmt->sharp && fmt->type != 'p')
		{
			if (fmt->type == 'o' || fmt->type == 'O')
				s_len = ft_strlen(s + 1);
			else
				s_len = ft_strlen(s);
		}
		else
			s_len = (!ft_strcmp(s, "0")) ? 3 : ft_strlen(s) - 2;
	}
	return (s_len);
}

static char	*add_zero(t_fmt *fmt, char *s)
{
	char	*tmp;
	char	*new;
	int		s_len;

	tmp = NULL;
	if ((s_len = len_zero(fmt, s)) == -1 && fmt->type != 'p')
	{
		return (ft_strdup(""));
	}
	if (s_len < fmt->prec_p)
	{
		tmp = ft_strnew(fmt->prec_p - s_len + 1);
		ft_memset(tmp, '0', fmt->prec_p - s_len);
		tmp[fmt->prec_p - s_len] = 0;
		new = ft_strjoin(tmp, s);
	}
	else
		new = ft_strdup(s);
	return (new);
}

static char	*add_signe(t_fmt *fmt, char *s)
{
	char	*new;

	if (fmt->type == 'p' && !ft_strcmp(s, "0") && fmt->is_point && !fmt->prec_p)
		new = ft_strdup("0x");
	else if ((ft_strcmp(s, "0") && fmt->sharp) || fmt->type == 'p')
	{
		if (fmt->type == 'x' || fmt->type == 'X' || fmt->type == 'p')
			new = ft_strjoin((fmt->type == 'X') ? "0X" : "0x", s);
		else
			new = ft_strjoin("0", s);
	}
	else
		new = ft_strdup(s);
	return (new);
}

static char	*add_esp(t_fmt *fmt, char *s)
{
	char	*new;
	char	*tmp;
	int		s_len;

	if (fmt->type != 'p' && fmt->is_point && !fmt->prec_p &&
	(!fmt->sharp || fmt->type == 'x' || fmt->type == 'X'))
		return (NULL);
	tmp = NULL;
	if (fmt->esp && fmt->l_r)
		s = ft_strjoin(" ", s);
	s_len = ft_strlen(s);
	if (s_len < fmt->prec_esp)
	{
		tmp = ft_strnew(fmt->prec_esp - s_len + 1);
		tmp = ft_memset(tmp, ' ', fmt->prec_esp - s_len);
		tmp[fmt->prec_esp - s_len] = 0;
		if (*s)
			new = (fmt->l_r) ? ft_strjoin(s, tmp) : ft_strjoin(tmp, s);
		else
			new = ft_strdup(tmp);
	}
	else
		new = ft_strdup(s);
	return (new);
}

char		*add_prec_onbr(t_fmt *fmt, char *s)
{
	char	*tmp;
	char	*new;

	new = NULL;
	if ((tmp = add_zero(fmt, s)))
		new = add_signe(fmt, tmp);
	tmp = new;
	new = add_esp(fmt, tmp);
	return (new);
}
