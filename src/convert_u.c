/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_u.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 14:47:01 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/22 20:35:56 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		**type_u(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if (!(nbr = (char **)malloc(sizeof(char *) * 1)))
		return (NULL);
	if ((fmt->type == 'U' && !fmt->l && !fmt->h) ||
			(fmt->type != 'U' && fmt->l == 1))
		*nbr = ft_ultoa(va_arg(pa, unsigned long int));
	else if ((fmt->type != 'U' && fmt->h == 1) ||
			(fmt->type == 'U' && fmt->h == 2))
		*nbr = ft_ustoa((short int)va_arg(pa, unsigned int));
	else if (fmt->type != 'U' && fmt->h == 2)
		*nbr = ft_uctoa((char)va_arg(pa, unsigned int));
	else if (fmt->z || (fmt->type == 'U' && fmt->l == 1) ||
			(fmt->type != 'U' && (fmt->l == 2 || fmt->j)))
		*nbr = ft_ulltoa(va_arg(pa, unsigned long long));
	else
		*nbr = ft_uitoa(va_arg(pa, unsigned int));
	return (nbr);
}

char		**convert_u(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if ((nbr = type_u(pa, fmt)))
		return (nbr);
	return (NULL);
}
