/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_d.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/06 10:36:38 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 17:04:48 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	**signe_nb(t_fmt *fmt, char **nbr)
{
	char	*s;

	if (nbr[0][0] != '-' && !fmt->z)
	{
		s = ft_strjoin("+", nbr[0]);
		ft_strdel(&(nbr[0]));
		nbr[0] = s;
	}
	nbr[1] = 0;
	return (nbr);
}

char		**type_d(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if (!(nbr = (char **)malloc(sizeof(char *) * 2)))
		return (NULL);
	if ((fmt->type == 'D' && !fmt->l && !fmt->h) ||
	(fmt->type != 'D' && fmt->l == 1))
		nbr[0] = ft_ltoa(va_arg(pa, long int));
	else if (fmt->type != 'D' && fmt->h == 1)
		nbr[0] = ft_stoa((short int)va_arg(pa, int));
	else if (fmt->type == 'D' && fmt->h == 2)
		nbr[0] = ft_ustoa((short int)va_arg(pa, int));
	else if (fmt->type != 'D' && fmt->h == 2)
		nbr[0] = ft_ctoa((char)va_arg(pa, int));
	else if ((fmt->type == 'D' && fmt->l == 1) ||
	(fmt->type != 'D' && (fmt->l == 2 || fmt->j)))
		nbr[0] = ft_lltoa(va_arg(pa, long long));
	else if (fmt->z)
		nbr[0] = ft_lltoa(va_arg(pa, long long));
	else
		nbr[0] = ft_itoa(va_arg(pa, int));
	if (!*nbr)
		return (nbr);
	return (signe_nb(fmt, nbr));
}

char		**convert_d(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if ((nbr = type_d(pa, fmt)))
		return (nbr);
	return (NULL);
}
