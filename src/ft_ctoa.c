/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ctoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 14:35:18 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 16:43:57 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_nbr(char n, char *s, int len)
{
	if (n < 0)
	{
		*s = '-';
		n *= -1;
	}
	if (n > 9)
		ft_nbr(n / 10, s, len - 1);
	*(s + len) = n % 10 + '0';
}

char		*ft_ctoa(char n)
{
	char	tmp;
	char	len;
	char	*new;

	tmp = n;
	len = 0;
	if (n == -128)
		return (ft_strdup("-128"));
	if (n < 0)
	{
		len++;
		tmp *= -1;
	}
	while (tmp > 9)
	{
		tmp /= 10;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
