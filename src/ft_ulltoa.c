/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/08 16:15:22 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 16:51:43 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_nbr(unsigned long long n, char *s, unsigned int len)
{
	if (n > 9)
		ft_nbr(n / 10, s, len - 1);
	*(s + len) = n % 10 + '0';
}

char					*ft_ulltoa(unsigned long long n)
{
	unsigned long long	tmp;
	int					len;
	char				*new;

	tmp = n;
	len = 0;
	while (tmp > 9)
	{
		tmp /= 10;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
