/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/05 13:08:05 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 22:10:22 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	total_len(char **tab)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	if (tab)
	{
		while (tab[i])
		{
			if (tab[i][0] == 0)
				len++;
			len += ft_strlen(tab[i]);
			i++;
		}
	}
	return (len);
}

int			new_arg(va_list pa, char *s, int i, char ***elem)
{
	if (s[i] == '%')
	{
		if (!(*elem = (char **)malloc(sizeof(char *) * 2)))
			return (i);
		elem[0][0] = ft_strdup("%");
		elem[0][1] = 0;
		i++;
	}
	else
		i += parse_arg(pa, s, i, elem);
	return (i);
}

char		**complete_string(va_list pa, char **prev, char *s, int *i)
{
	char	**tmp;
	char	**new;
	char	**elem;

	if (!(tmp = ft_newtab(1)))
		return (NULL);
	if ((*i - 1) >= 0)
		tmp[0] = ft_strndup(s, *i - 1);
	*i += 1;
	if (prev && *prev && tmp && *tmp)
		tmp = realloc_tab(prev, tmp);
	*i = new_arg(pa, s, *i, &elem);
	if (elem && *elem)
	{
		if ((tmp && *tmp) || (prev && *prev))
			new = realloc_tab((tmp && *tmp) ? tmp : prev, elem);
		else
			new = realloc_tab(elem, NULL);
	}
	else
		new = realloc_tab((*tmp) ? tmp : prev, NULL);
	return (new);
}

static char	**convert_all(va_list pa, char *s, int i)
{
	char	**tmp;
	char	**new;

	new = NULL;
	if (!(tmp = ft_newtab(1)))
		return (NULL);
	while (s[i])
	{
		if (s[i] == '%')
		{
			if (!s[i + 1])
				break ;
			new = complete_string(pa, new, s, &i);
			s = s + i;
			i = 0;
		}
		if (s[i] && !s[i + 1])
		{
			*tmp = ft_strdup(s);
			new = (new) ? realloc_tab(new, tmp) : NULL;
			break ;
		}
		i = (s[i] && s[i] != '%' && s[i + 1]) ? i + 1 : i;
	}
	return ((new) ? new : tmp);
}

int			ft_printf(char *s, ...)
{
	va_list	pa;
	char	**new;
	int		i;

	i = 0;
	va_start(pa, s);
	new = NULL;
	if (!s || !*s)
		return (0);
	if (!(new = convert_all(pa, s, i)))
		return (0);
	va_end(pa);
	aff_tab(new);
	return (total_len(new));
}
