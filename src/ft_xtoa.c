/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xtoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 13:07:28 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 16:55:27 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_nbr(unsigned int n, char *s, int len)
{
	if (n > 15)
		ft_nbr(n / 16, s, len - 1);
	if (n % 16 > 9)
		*(s + len) = n % 16 % 10 + 'a';
	else
		*(s + len) = n % 16 + '0';
}

char		*ft_xtoa(unsigned int n)
{
	unsigned int	tmp;
	int				len;
	char			*new;

	tmp = n;
	len = 0;
	while (tmp > 15)
	{
		tmp /= 16;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
