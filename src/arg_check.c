/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arg_check.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/04 22:30:36 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 20:10:16 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	find_type(t_fmt *fmt, char s)
{
	if (s == 'd' || s == 'D' || s == 'i' || s == 's' || s == 'S' || s == 'x' ||
		s == 'X' || s == 'o' || s == 'O' || s == 'u' || s == 'U' || s == 'p' ||
		s == 'c' || s == 'C')
	{
		fmt->type = s;
		return (1);
	}
	return (0);
}

static int	find_type_flags(t_fmt *fmt, char *s)
{
	if (s[0] == 'l' || s[0] == 'h' || s[0] == 'j' || s[0] == 'z')
	{
		fmt->l = 0;
		fmt->h = 0;
		fmt->j = 0;
		fmt->z = 0;
	}
	if (s[0] == 'l')
		return ((fmt->l = (s[1] == 'l') ? 2 : 1));
	else if (s[0] == 'h')
		return ((fmt->h = (s[1] == 'h') ? 2 : 1));
	else if (s[0] == 'j')
		return ((fmt->j = 1));
	else if (s[0] == 'z')
		return ((fmt->z = 1));
	return (0);
}

static int	fill_prec(t_fmt *fmt, char *s, int j)
{
	while (ft_isdigit(s[j]) && s[1] != '0')
		j++;
	if (s[0] == '.' || s[0] == '0')
	{
		if (s[0] == '.')
		{
			fmt->is_point = 1;
			if (fmt->zero)
				fmt->prec_esp = fmt->prec_p;
		}
		else if (s[0] == '0')
			fmt->zero = 1;
		fmt->prec_p = (j == 1) ? 0 : ft_atoi(s + 1);
	}
	else
	{
		if (s[0] == ' ')
			fmt->esp = 1;
		else if (s[0] == '-')
			fmt->l_r = 1;
		fmt->prec_esp = (j == 1) ? 0 : ft_atoi(s + 1);
	}
	return (j);
}

static int	fill_fmt(t_fmt *fmt, char *s)
{
	int		j;

	j = 0;
	if (s[0] == '.' || s[0] == '0' || s[0] == ' ' || s[0] == '-')
	{
		return (fill_prec(fmt, s, j + 1));
	}
	else if (s[0] == '#' || s[0] == '+')
	{
		if (s[0] == '#')
			fmt->sharp = 1;
		else if (s[0] == '+')
			fmt->signe = 1;
		j = 1;
	}
	return (j);
}

int			check_arg(t_fmt *fmt, char *s)
{
	int		len;
	int		i;
	int		j;

	i = 0;
	len = 1;
	while (len)
	{
		len = 0;
		j = 0;
		len += fill_fmt(fmt, s + i);
		len += find_type_flags(fmt, s + i);
		if (ft_isdigit(s[i + len]) && s[i + len] != '0')
		{
			fmt->prec_esp = 0;
			while (ft_isdigit(s[i + j]))
				j++;
			fmt->prec_esp = ft_atoi(s + i);
			len += j;
		}
		if (find_type(fmt, s[i]))
			return (i + 1);
		i += len;
	}
	return (i);
}
