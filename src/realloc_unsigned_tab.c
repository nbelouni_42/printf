/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc_unsigned_tab.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/20 11:12:02 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 18:14:43 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

typedef unsigned char	t_uchar;

static int		total_size(t_uchar **tab, t_uchar **elem)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (tab && tab[i])
		i++;
	while (elem && elem[j])
		j++;
	return (i + j);
}

static int		s_len(t_uchar *s)
{
	int			len;

	len = 0;
	while (s[len])
		len++;
	return (len);
}

static t_uchar	**cp_tab(t_uchar **new, t_uchar **tab, t_uchar **elem, int size)
{
	int		i;
	int		j;
	int		len;

	i = -1;
	j = -1;
	while (tab && tab[++i])
	{
		len = s_len(tab[i]);
		if (!(new[i] = (t_uchar *)malloc(sizeof(t_uchar) * len + 1)))
			return (NULL);
		ft_memcpy(new[i], tab[i], len);
		new[i][len] = 0;
	}
	while (elem && i < size)
	{
		len = s_len(elem[++j]);
		if (!(new[i] = (t_uchar *)malloc(sizeof(t_uchar) * len + 1)))
			return (NULL);
		ft_memcpy(new[i], elem[j], len);
		new[i][len] = 0;
		i++;
	}
	new[i] = 0;
	return (new);
}

unsigned char	**realloc_utab(unsigned char **tab, unsigned char **elem)
{
	t_uchar	**new;
	int		size;

	size = total_size(tab, elem);
	if (!(new = (t_uchar **)malloc(sizeof(t_uchar *) * (size + 1))))
		return (NULL);
	new = cp_tab(new, tab, elem, size);
	return (new);
}
