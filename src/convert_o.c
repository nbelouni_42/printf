/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_o.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 13:50:45 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/22 20:13:29 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		**type_o(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if (!(nbr = (char **)malloc(sizeof(char *) * 1)))
		return (NULL);
	if ((fmt->type == 'O' && !fmt->l && !fmt->h) ||
			(fmt->type != 'O' && fmt->l == 1))
		*nbr = ft_oltoa(va_arg(pa, unsigned long int));
	else if ((fmt->type != 'O' && fmt->h == 1) ||
			(fmt->type == 'O' && fmt->h == 2))
		*nbr = ft_ostoa((short int)va_arg(pa, unsigned int));
	else if (fmt->type != 'O' && fmt->h == 2)
		*nbr = ft_octoa((char)va_arg(pa, unsigned int));
	else if (fmt->z || (fmt->type == 'O' && fmt->l == 1) ||
			(fmt->type != 'O' && (fmt->l == 2 || fmt->j)))
		*nbr = ft_olltoa(va_arg(pa, unsigned long long));
	else
		*nbr = ft_otoa(va_arg(pa, unsigned int));
	return (nbr);
}

char		**convert_o(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if ((nbr = type_o(pa, fmt)))
		return (nbr);
	return (NULL);
}
