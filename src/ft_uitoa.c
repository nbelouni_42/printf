/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/06 18:55:16 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/08 16:06:30 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void			ft_nbr(unsigned int n, char *s, unsigned int len)
{
	if (n > 9)
		ft_nbr(n / 10, s, len - 1);
	*(s + len) = n % 10 + '0';
}

char				*ft_uitoa(unsigned int n)
{
	unsigned int	tmp;
	int				len;
	char			*new;

	tmp = n;
	len = 0;
	while (tmp > 9)
	{
		tmp /= 10;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
