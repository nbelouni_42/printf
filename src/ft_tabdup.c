/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 17:06:58 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 17:10:39 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_tabdup(char **tab)
{
	char	**new;
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (tab && tab[i])
		i++;
	if (!(new = (char **)malloc(sizeof(char *) * (i + 1))))
		return (NULL);
	while (j < i)
	{
		new[j] = ft_strdup(tab[j]);
		j++;
	}
	new[j] = 0;
	return (new);
}
