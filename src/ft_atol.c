/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/05 01:55:43 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/05 01:55:46 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static long int		white(const char *str)
{
	int			i;

	i = 0;
	if (str[i] == '\n' || str[i] == '\f' || str[i] == '\v' ||
		str[i] == '\r' || str[i] == '\t' || str[i] == ' ')
		while (str[i] == '\n' || str[i] == '\f' || str[i] == '\v' ||
				str[i] == '\r' || str[i] == '\t' || str[i] == ' ')
			i++;
	return (i);
}

long int			ft_atol(const char *str)
{
	long int		nbr;
	long int		i;
	long int		ref;

	nbr = 0;
	ref = 0;
	i = white(str);
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			ref = 1;
		i++;
	}
	while (str[i] != '\0' && (str[i] >= '0' && str[i] <= '9'))
	{
		nbr = (nbr * 10) + (str[i] - '0');
		i++;
	}
	if (ref != 0)
		nbr *= -1;
	return (nbr);
}
