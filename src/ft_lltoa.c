/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 03:07:06 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 16:45:04 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void			ft_nbr(long long n, char *s, long long len)
{
	if (n < 0)
	{
		*s = '-';
		n *= -1;
	}
	if (n > 9)
		ft_nbr(n / 10, s, len - 1);
	*(s + len) = n % 10 + '0';
}

static char			*exeption(void)
{
	char			*new;

	new = (char *)malloc(sizeof(char) * 21);
	if (!new)
		return (NULL);
	ft_nbr(-922337203685477580, new, 18);
	new[19] = '8';
	new[20] = '\0';
	return (new);
}

char				*ft_lltoa(long long n)
{
	long long		tmp;
	long long		len;
	char			*new;

	tmp = n;
	len = 0;
	if (n == -9223372036854775807 - 1)
		return (exeption());
	if (n < 0)
	{
		len++;
		tmp *= -1;
	}
	while (tmp > 9)
	{
		tmp /= 10;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
