/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_x.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 13:00:21 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/22 20:38:18 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*ft_strupper(char *s)
{
	int		i;

	i = 0;
	while (s[i])
	{
		if (s[i] >= 'a' && s[i] <= 'z')
			s[i] -= 32;
		i++;
	}
	return (s);
}

char		**type_x(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if (!(nbr = (char **)malloc(sizeof(char *) * 2)))
		return (NULL);
	if (fmt->type == 'p')
		*nbr = ft_xlltoa(va_arg(pa, unsigned long long));
	else if (fmt->l == 1)
		*nbr = ft_xltoa(va_arg(pa, unsigned long int));
	else if (fmt->h == 1)
		*nbr = ft_xstoa((unsigned short int)va_arg(pa, int));
	else if (fmt->h == 2)
		*nbr = ft_xctoa((unsigned char)va_arg(pa, int));
	else if (fmt->l == 2 || fmt->j)
		*nbr = ft_xlltoa(va_arg(pa, unsigned long long));
	else if (fmt->z)
		*nbr = ft_xltoa(va_arg(pa, unsigned long int));
	else
		*nbr = ft_xtoa(va_arg(pa, unsigned int));
	if (fmt->type == 'X')
		*nbr = ft_strupper(*nbr);
	nbr[1] = 0;
	return (nbr);
}

char		**convert_x(va_list pa, t_fmt *fmt)
{
	char	**nbr;

	if ((nbr = type_x(pa, fmt)))
		return (nbr);
	return (NULL);
}
