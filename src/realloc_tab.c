/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/18 13:23:55 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 18:15:43 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				total_size(char **tab, char **elem)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (tab && tab[i])
		i++;
	while (elem && elem[j])
		j++;
	return (i + j);
}

static char		**cp_tab(char **new, char **tab, char **elem, int size)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (tab && tab[i])
	{
		new[i] = ft_strdup(tab[i]);
		i++;
	}
	while (elem && i < size)
	{
		new[i] = ft_strdup(elem[j]);
		j++;
		i++;
	}
	new[i] = 0;
	return (new);
}

char			**realloc_tab(char **tab, char **elem)
{
	char	**new;
	int		size;

	size = total_size(tab, elem);
	if (!(new = (char **)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	new = cp_tab(new, tab, elem, size);
	return (new);
}
