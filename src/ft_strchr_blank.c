/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_blank.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/21 09:19:46 by nbelouni          #+#    #+#             */
/*   Updated: 2015/01/28 13:58:42 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*strchr_blank(char *s)
{
	int		i;
	char	*new;

	i = 0;
	while (s[i])
	{
		if (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
			break ;
		i++;
	}
	if (!(new = (char *)malloc(sizeof(char) * i)))
		return (NULL);
	ft_strncpy(new, s, i);
	new[i] = '\0';
	return (new);
}
