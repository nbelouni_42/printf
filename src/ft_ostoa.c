/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ostoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 13:53:44 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/12 13:54:07 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_nbr(unsigned short int n, char *s, int len)
{
	if (n > 7)
		ft_nbr(n / 8, s, len - 1);
	*(s + len) = n % 8 + '0';
}

char					*ft_ostoa(unsigned short int n)
{
	unsigned short int	tmp;
	int					len;
	char				*new;

	tmp = n;
	len = 0;
	while (tmp > 7)
	{
		tmp /= 8;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
