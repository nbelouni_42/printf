/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format_convert.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/06 16:49:35 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 18:29:32 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_fmt			*init_fmt(void)
{
	t_fmt	*fmt;

	if (!(fmt = (t_fmt *)malloc(sizeof(t_fmt))))
		return (NULL);
	fmt->type = 0;
	fmt->is_point = 0;
	fmt->prec_p = 0;
	fmt->prec_esp = 0;
	fmt->zero = 0;
	fmt->esp = 0;
	fmt->l = 0;
	fmt->h = 0;
	fmt->j = 0;
	fmt->z = 0;
	fmt->l_r = 0;
	fmt->sharp = 0;
	fmt->signe = 0;
	return (fmt);
}

static void		add_prec(t_fmt *fmt, char ***elem)
{
	if ((fmt->type == 'd' || fmt->type == 'D' || fmt->type == 'i') && !fmt->z)
		**elem = add_prec_nbr(fmt, **elem);
	else if (fmt->type == 'u' || fmt->type == 'U' || fmt->z)
		**elem = add_prec_unbr(fmt, **elem);
	else if (fmt->type == 'x' || fmt->type == 'X' || fmt->type == 'p' ||
	fmt->type == 'O' || fmt->type == 'o')
		**elem = add_prec_onbr(fmt, **elem);
	else if (**elem && (fmt->type == 'c' || fmt->type == 'C' ||
	fmt->type == 's' || fmt->type == 'S'))
		*elem = add_prec_str(fmt, *elem);
}

static void		convert(t_fmt *fmt, char ***elem, va_list pa)
{
	if (fmt->type == 'd' || fmt->type == 'D' || fmt->type == 'i')
		*elem = convert_d(pa, fmt);
	else if (fmt->type == 's' || fmt->type == 'S')
	{
		if (!(*elem = convert_s(pa, fmt)))
			return ;
	}
	else if (fmt->type == 'x' || fmt->type == 'X' || fmt->type == 'p')
		*elem = convert_x(pa, fmt);
	else if (fmt->type == 'o' || fmt->type == 'O')
		*elem = convert_o(pa, fmt);
	else if (fmt->type == 'u' || fmt->type == 'U')
		*elem = convert_u(pa, fmt);
	else if (fmt->type == 'c' || fmt->type == 'C')
		*elem = convert_c(pa, fmt);
}

int				parse_arg(va_list pa, char *s, int i, char ***elem)
{
	t_fmt	*fmt;
	int		j;

	*elem = NULL;
	fmt = init_fmt();
	j = check_arg(fmt, s + i);
	if (fmt->type)
		convert(fmt, elem, pa);
	else
	{
		fmt->type = 'c';
		if (!(*elem = (char **)malloc(sizeof(char *) * 2)))
			return (j);
		elem[0][0] = ft_strndup(s + i + j, 0);
		j++;
	}
	if (!*elem)
		return (j);
	add_prec(fmt, elem);
	return (j);
}
