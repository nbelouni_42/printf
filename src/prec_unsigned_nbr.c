/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prec_unsigned_nbr.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/04 17:08:46 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 20:01:42 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	len_zero(t_fmt *fmt, char *s)
{
	int		s_len;

	s_len = 0;
	if (fmt->is_point)
	{
		if (!ft_strcmp(s, "0") && !fmt->prec_p)
			s_len = -1;
		else
			s_len = ft_strlen(s);
	}
	return (s_len);
}

static char	*add_zero(t_fmt *fmt, char *s)
{
	char	*tmp;
	char	*new;
	int		s_len;

	tmp = NULL;
	if ((s_len = len_zero(fmt, s)) == -1)
		return (ft_strdup(""));
	if (s_len < fmt->prec_p)
	{
		tmp = ft_strnew(fmt->prec_p - s_len + 1);
		ft_memset(tmp, '0', fmt->prec_p - s_len);
		tmp[fmt->prec_p - s_len] = 0;
		new = ft_strjoin(tmp, s);
	}
	else
		new = ft_strdup(s);
	if (tmp && *tmp)
		ft_strdel(&tmp);
	return (new);
}

static char	*add_esp(t_fmt *fmt, char *s)
{
	char	*new;
	char	*tmp;
	int		s_len;

	tmp = NULL;
	if ((fmt->is_point || fmt->zero) && !fmt->prec_p && !*s)
		return (NULL);
	if (fmt->esp && fmt->l_r)
		s = ft_strjoin(" ", s);
	s_len = ft_strlen(s);
	if (s_len < fmt->prec_esp)
	{
		tmp = ft_strnew(fmt->prec_esp - s_len);
		tmp = ft_memset(tmp, ' ', fmt->prec_esp - s_len);
		tmp[fmt->prec_esp - s_len] = 0;
		if (*s)
			new = (fmt->l_r) ? ft_strjoin(s, tmp) : ft_strjoin(tmp, s);
		else
			new = ft_strdup(tmp);
	}
	else
		new = ft_strdup(s);
	if (tmp)
		ft_strdel(&tmp);
	return (new);
}

char		*add_prec_unbr(t_fmt *fmt, char *s)
{
	char	*tmp;
	char	*new;

	tmp = add_zero(fmt, s);
	new = add_esp(fmt, tmp);
	ft_strdel(&tmp);
	return (new);
}
