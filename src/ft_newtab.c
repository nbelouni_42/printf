/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_newtab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 19:54:28 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 19:57:46 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_newtab(int size)
{
	char	**new;

	if (!(new = (char **)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	while (size >= 0)
	{
		new[size] = 0;
		size--;
	}
	return (new);
}
