/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_c.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 17:07:52 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 17:00:40 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			wchar_to_char(unsigned char **s, wchar_t c)
{
	if (c < (1 << 7))
		*(*s)++ = (unsigned char)(c);
	else if (c < (1 << 11))
	{
		*(*s)++ = (unsigned char)((c >> 6) | 0xC0);
		*(*s)++ = (unsigned char)((c & 0x3F) | 0x80);
		return (2);
	}
	else if (c < (1 << 16))
	{
		*(*s)++ = (unsigned char)(((c >> 12)) | 0xE0);
		*(*s)++ = (unsigned char)(((c >> 6) & 0x3F) | 0x80);
		*(*s)++ = (unsigned char)((c & 0x3F) | 0x80);
		return (3);
	}
	else if (c < (1 << 21))
	{
		*(*s)++ = (unsigned char)(((c >> 18)) | 0xF0);
		*(*s)++ = (unsigned char)(((c >> 12) & 0x3F) | 0x80);
		*(*s)++ = (unsigned char)(((c >> 6) & 0x3F) | 0x80);
		*(*s)++ = (unsigned char)((c & 0x3F) | 0x80);
		return (4);
	}
	return (1);
}

char		*cast_to_wc(wchar_t s, int *len)
{
	unsigned char	wchar[1024];
	unsigned char	*b;
	char			*str;
	wchar_t			c;

	b = wchar;
	if (!s)
		return (NULL);
	c = s;
	if ((*len = wchar_to_char(&b, c)) == -1)
		return (NULL);
	str = (char *)wchar;
	return (str);
}

char		**type_c(va_list pa, t_fmt *fmt, int *len)
{
	char	**s;
	char	*w_tmp;
	wchar_t	w;
	char	c;

	if (!(s = (char **)malloc(sizeof(char *) * 2)))
		return (NULL);
	c = 0;
	s[0] = 0;
	if ((fmt->type == 'c' && fmt->l == 1) || fmt->type == 'C')
	{
		if ((w = va_arg(pa, wchar_t)))
		{
			w_tmp = cast_to_wc(w, len);
			s[0] = ft_strndup(w_tmp, (*len) - 1);
		}
	}
	else if ((c = (char)va_arg(pa, int)))
		s[0] = ft_strndup(&c, 0);
	if (!s[0])
		s[0] = ft_strdup("");
	s[1] = 0;
	return (s);
}

char		**convert_c(va_list pa, t_fmt *fmt)
{
	char	**s;
	int		len;

	len = 0;
	s = type_c(pa, fmt, &len);
	if (s)
		return (s);
	return (NULL);
}
