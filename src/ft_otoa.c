/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 13:49:38 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/12 13:50:23 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_nbr(unsigned int n, char *s, int len)
{
	if (n > 7)
		ft_nbr(n / 8, s, len - 1);
	*(s + len) = n % 8 + '0';
}

char		*ft_otoa(unsigned int n)
{
	unsigned int	tmp;
	int				len;
	char			*new;

	tmp = n;
	len = 0;
	while (tmp > 7)
	{
		tmp /= 8;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
