/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prec_sort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 18:48:17 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 20:00:19 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char		**sort_zero(char **tmp, char **new)
{
	if (tmp[1])
	{
		new[0] = tmp[1];
		new[1] = tmp[0];
		new[2] = tmp[2];
		new[3] = 0;
	}
	else
	{
		new[0] = tmp[0];
		new[1] = tmp[2];
		new[2] = 0;
	}
	return (new);
}

static char		**sort_esp(t_fmt *fmt, char **tmp, char **new)
{
	new[0] = tmp[2];
	new[1] = 0;
	if (tmp[1])
	{
		new[1] = tmp[1];
		new[2] = tmp[0];
		new[3] = 0;
	}
	else if ((tmp[0] && tmp[0][0] != 0) ||
			(fmt->type == 'c' || fmt->type == 'C'))
	{
		new[1] = tmp[0];
		new[2] = 0;
	}
	return (new);
}

char			**prec_sort(t_fmt *fmt, char **tmp, char **new)
{
	if (fmt->l_r && tmp[2])
		return (sort_zero(tmp, new));
	else
	{
		if (tmp[2])
			return (sort_esp(fmt, tmp, new));
		else if (tmp[1])
		{
			new[0] = tmp[1];
			if ((fmt->type != 'c' && fmt->type != 'C') &&
			fmt->is_point && !fmt->prec_p)
				new[1] = 0;
			else
			{
				new[1] = tmp[0];
				new[2] = 0;
			}
		}
		else
		{
			new[0] = tmp[0];
			new[1] = 0;
		}
	}
	return (new);
}
