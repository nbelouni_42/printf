/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_oltoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/12 13:52:13 by nbelouni          #+#    #+#             */
/*   Updated: 2015/02/12 13:52:15 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_nbr(unsigned long int n, char *s, int len)
{
	if (n > 7)
		ft_nbr(n / 8, s, len - 1);
	*(s + len) = n % 8 + '0';
}

char					*ft_oltoa(unsigned long int n)
{
	unsigned long int	tmp;
	int					len;
	char				*new;

	tmp = n;
	len = 0;
	while (tmp > 7)
	{
		tmp /= 8;
		len++;
	}
	new = (char *)malloc(sizeof(char) * len + 2);
	if (!new)
		return (NULL);
	ft_nbr(n, new, len);
	new[len + 1] = '\0';
	return (new);
}
