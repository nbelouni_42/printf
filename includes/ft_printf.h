/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nbelouni <nbelouni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/05 13:01:21 by nbelouni          #+#    #+#             */
/*   Updated: 2015/04/29 22:12:54 by nbelouni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include "libft.h"

typedef struct	s_fmt
{
	char		type;

	int			is_point;
	int			prec_p;
	int			prec_esp;
	int			zero;
	int			esp;

	int			l;
	int			ll;
	int			h;
	int			hh;
	int			j;
	int			z;

	int			l_r;
	int			sharp;
	int			signe;
}				t_fmt;

int				ft_printf(char *s, ...);
int				parse_arg(va_list pa, char *s, int i, char ***elem);
char			**convert_d(va_list pa, t_fmt *fmt);
char			**convert_s(va_list pa, t_fmt *fmt);
char			**convert_x(va_list pa, t_fmt *fmt);
char			**convert_o(va_list pa, t_fmt *fmt);
char			**convert_u(va_list pa, t_fmt *fmt);
char			**convert_c(va_list pa, t_fmt *fmt);
char			*add_prec_nbr(t_fmt *fmt, char *s);
char			*add_prec_unbr(t_fmt *fmt, char *s);
char			*add_prec_onbr(t_fmt *fmt, char *s);
char			**prec_sort(t_fmt *fmt, char **tmp, char **new);
char			**add_prec_str(t_fmt *fmt, char **s);
int				check_arg(t_fmt *fmt, char *s);
char			**realloc_tab(char **tab, char **elem);
unsigned char	**realloc_utab(unsigned char **tab, unsigned char **elem);

#endif
